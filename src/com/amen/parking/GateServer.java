package com.amen.parking;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.amen.parking.interfaces.IGateEventListener;
import com.amen.parking.interfaces.IRate;
import com.amen.parking.interfaces.ITicketManager;
import com.amen.parking.interfaces.Rate;
import com.amen.parking.interfaces.Rate.RateType;
import com.amen.parking.model.CarDatabase;
import com.amen.parking.model.FreeRate;
import com.amen.parking.model.Gate;
import com.amen.parking.model.ParkingInfo;
import com.amen.parking.model.Ticket;
import com.amen.parking.model.WeekRate;

public class GateServer implements ITicketManager {

	// private RateType currentRate;
	// Cena po której rozliczane są samochody w kasach (strategia)
	private IRate currentRate = new FreeRate();
	// nasłuchuje zdarzeń bram
	private IGateEventListener gateEventListener;

	// mapa nazwa_parkingu -> informacje o parkingu
	private Map<String, ParkingInfo> parkings = new HashMap<>();
	// mapa brama -> parking którego dotyczy (wjazd na jaki parking)
	private Map<Gate, ParkingInfo> gatesToParking = new HashMap<>();

	/**
	 * Konstruktor 
	 * @param gateEventListener - przekazujemy obiekt nasłuchujący zdarzeń bramy
	 */
	public GateServer(IGateEventListener gateEventListener) {
		super();
		this.gateEventListener = gateEventListener;
	}

	/**
	 * Dodanie parkingu
	 * @param name - o nazwie
	 * @param capacity - z taką pojemnością
	 */
	public void addParking(String name, int capacity) {
		System.out.println("Parking " + name + " added");
		parkings.put(name, new ParkingInfo(capacity));
	}

	/**
	 * Dodanie bramy
	 * @param gateId - id gate'u (łatwiej się wpisuje z linii polecen)
	 * @param parkingName - nazwa parkingu.
	 */
	public void addGate(int gateId, String parkingName) {
		if (parkings.containsKey(parkingName)) { // jesli parking o danej nazwie istnieje 
			gatesToParking.put(new Gate(gateId, this), parkings.get(parkingName)); //dodaj 
			System.out.println("Gate " + gateId + " added");
		} else {
			System.out.println("Gate NOT " + gateId + " added");
		}
	}

	/**
	 * Optional wykorzystanie klasy dzięki której nie musimy używać nulla. 
	 * Optional oznacza, że ta metoda może zwrócić obiekt (taki jaki w nawiasach)
	 * ale nie musi. Możemy to sprawdzić metodą isPresent().
	 */
	@Override
	public Optional<Ticket> tryGenerateTicket(Gate gate, String reg) {
		ParkingInfo parking = gatesToParking.get(gate);
		if (parking.hasFreeSpot()) { // jeśli parking ma wolne miejsce
			Ticket t = new Ticket(currentRate.getRate());
			// Ticket t = new Ticket(Rate.countRate(currentRate)); //
			// currentRate to enum
			parking.addTicket(t); // dodaj ticket
			System.out.println("Ticket " + t.getId() + " generated");

			gateEventListener.in(t.getId(), reg); // zarejestruj zmianę w bramkach
			return Optional.of(t); // zwróć optional
		}
		System.out.println("Unable to generate ticket");
		return Optional.empty(); // null - brak optionala.
	}

	public void setRate(IRate rate) { // ustawienie strategia
		this.currentRate = rate;
	}

	/**
	 * Walidacja ticketu. Rozliczanie klientów.
	 */
	@Override
	public boolean tryValidate(Gate gate, int ticketId, String reg) {
		System.out.println("try Validating");

		ParkingInfo parking = gatesToParking.get(gate);
		Ticket toValidate = parking.getTicketInfo(ticketId);
		if(toValidate == null){
			return false;
		}
		if (!toValidate.isPaid()) {
			toValidate.setTimestampPay(System.currentTimeMillis());
			long stayLength = toValidate.getStayLength();
			double payAmount = stayLength * toValidate.getTicketRate();

			gateEventListener.out(ticketId, reg); // rejestracja sprawdzana z ticketem
			// jeśli ticket i resetracja nie są takie jakie wygenerowano na wjeździe, to 
			// wzbudzi się alarm.
			
			System.out.println("Amount to pay is: " + payAmount);
			return payAmount == 0.0;
		}
		return true;
	}

	// tylko do testowania
	public Gate getGate(int gateNumber) {
		for (Gate g : gatesToParking.keySet()) {
			if (g.getId() == gateNumber) {
				return g;
			}
		}
		return null;
	}

	/**
	 * Druga metoda do rozliczenia ticketu.
	 */
	@Override
	public void validate(Gate gate, int ticketId) {
		ParkingInfo parking = gatesToParking.get(gate);
		Ticket toValidate = parking.getTicketInfo(ticketId);
		toValidate.setTimestampOut(System.currentTimeMillis());
		if (!toValidate.isPaid()) {
			long stayLength = toValidate.getStayLength();
			double payAmount = stayLength * toValidate.getTicketRate();

			toValidate.setTimestampPay(System.currentTimeMillis());
			toValidate.setPaid(true);
			toValidate.setAmountToPay(payAmount);
		}
	}
}
