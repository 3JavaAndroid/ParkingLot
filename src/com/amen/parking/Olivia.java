package com.amen.parking;

import com.amen.parking.model.CarDatabase;
import com.amen.parking.model.SecurityGate;

/**
 * Klasa zewnętrzna spinająca klasy server i car database.
 * @author amen
 *
 */
public class Olivia {
	private GateServer serverGates;
	private CarDatabase carDb;

	public Olivia() {
		new SecurityGate();

		carDb = new CarDatabase();
		serverGates = new GateServer(carDb);
	}

	public GateServer getServerGates() {
		return serverGates;
	}

	public void setServerGates(GateServer serverGates) {
		this.serverGates = serverGates;
	}

	public void printCars() {
		carDb.printAll();
	}
}
