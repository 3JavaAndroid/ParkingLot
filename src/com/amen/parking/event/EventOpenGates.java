package com.amen.parking.event;

import java.util.List;

import com.amen.parking.interfaces.IGatesOpenEventListener;

public class EventOpenGates implements IEvent {

	/**
	 * Jedno ze zdarzeń - powoduje wysłanie do obiektów implementujacych interfejs 
	 * IGatesOpenEventListener zdarzenia - wywołanie na nich metody gatesOpen.
	 */
	@Override
	public void execute() {
		// obsługa zdarzenia
		List<IGatesOpenEventListener> listeners = (List<IGatesOpenEventListener>) EventDispatcher.INSTANCE
				.getAllObjectsImplementingInterface(IGatesOpenEventListener.class);

		for (IGatesOpenEventListener listener : listeners) {
			listener.gatesOpen();
		}

	}
}
