package com.amen.parking.event;

import java.util.List;

import com.amen.parking.interfaces.IGatesOpenEventListener;
import com.amen.parking.interfaces.ISecurityBreachListener;

public class EventSecurityBreached implements IEvent {

	/** 
	 * Podobnie jak EventOpenGates
	 */
	@Override
	public void execute() {
		List<ISecurityBreachListener> listeners = (List<ISecurityBreachListener>) EventDispatcher.INSTANCE
				.getAllObjectsImplementingInterface(ISecurityBreachListener.class);

		for (ISecurityBreachListener listener : listeners) {
			listener.securityBreached();
		}
	}
}
