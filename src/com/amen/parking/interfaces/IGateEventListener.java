package com.amen.parking.interfaces;

public interface IGateEventListener {
	void in(int ticketId, String reg);
	void out(int ticketId, String reg);
}
