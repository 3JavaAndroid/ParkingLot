package com.amen.parking.interfaces;

public interface ISecurityBreachListener {

	void securityBreached();
}
