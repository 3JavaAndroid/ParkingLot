package com.amen.parking.interfaces;

public interface IGatesOpenEventListener {

	void gatesOpen();
}
