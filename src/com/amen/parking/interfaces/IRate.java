package com.amen.parking.interfaces;

public interface IRate {
	double getRate();
}
