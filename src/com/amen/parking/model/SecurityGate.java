package com.amen.parking.model;

import com.amen.parking.event.EventDispatcher;
import com.amen.parking.interfaces.ISecurityBreachListener;

public class SecurityGate implements ISecurityBreachListener {

	public SecurityGate() {
		// Aby EventDispatcher informował o zdarzeniach, należy obiekty zarejestrować
		EventDispatcher.INSTANCE.registerObject(this);
	}

	@Override
	public void securityBreached() {
		System.err.println("Już tam śmigam!");
	}
}
