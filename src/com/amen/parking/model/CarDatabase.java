package com.amen.parking.model;

import java.security.KeyStore.Entry;
import java.util.HashMap;
import java.util.Map;

import com.amen.parking.event.EventDispatcher;
import com.amen.parking.event.EventSecurityBreached;
import com.amen.parking.interfaces.IGateEventListener;

public class CarDatabase implements IGateEventListener {
	/**
	 * Baza danych samochodów znajdujących się na parkingach.
	 */
	private Map<Integer, String> map = new HashMap<>();

	public CarDatabase() {
		EventDispatcher.INSTANCE.registerObject(this);
	}

	public void in(int ticketId, String reg) {
		map.put(ticketId, reg);
	}

	public void out(int ticketId, String reg) {
		if (!map.containsKey(ticketId) || !map.get(ticketId).equals(reg)) {
			EventDispatcher.INSTANCE.dispatchEvent(new EventSecurityBreached());
		} else {
			map.remove(ticketId);
		}
	}

	public void printAll() {
		System.out.println("Auta:");
		for (java.util.Map.Entry<Integer, String> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " -> " + entry.getValue());
		}
	}
}
