package com.amen.parking.model;

import com.amen.parking.interfaces.IRate;

public class WeekRate implements IRate {

	@Override
	public double getRate() {
		return 3.0;
	}
}
