package com.amen.parking.model;

import java.util.Optional;

import com.amen.parking.event.EventDispatcher;
import com.amen.parking.interfaces.IGatesOpenEventListener;
import com.amen.parking.interfaces.ITicketManager;

public class Gate implements IGatesOpenEventListener {

	private int id;
	private boolean isOpen;
	private ITicketManager ticketProvider;

	public Gate(int id, ITicketManager ticketProvider) {
		super();
		this.id = id;
		this.isOpen = true;
		this.ticketProvider = ticketProvider;
		
		// Aby EventDispatcher informował o zdarzeniach, należy obiekty zarejestrować
		EventDispatcher.INSTANCE.registerObject(this);
	}

	@Override
	public void gatesOpen() {
		System.out.println("Gate " + id + " is now opening.");
	}

	private Optional<Ticket> generateTicket(String registrationNumber) {
		return ticketProvider.tryGenerateTicket(this, registrationNumber);
	}

	public boolean checkIn(String registrationNumber) {
		if (!isOpen) {
			return false;
		}

		Optional<Ticket> generatedTicket = generateTicket(registrationNumber);
		if (generatedTicket.isPresent()) {
			System.out.println(generatedTicket.get());
			return true;
		}
		return false;
	}

	public boolean checkOut(int ticketId, String registrationNumber) {
		if (!ticketProvider.tryValidate(this, ticketId, registrationNumber)) {
			ticketProvider.validate(this, ticketId);
		}
		return true;
	}

	public int getId() {
		return id;
	}
}
