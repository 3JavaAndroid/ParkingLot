package com.amen.parking.model;

import com.amen.parking.interfaces.IRate;

public class NightRate implements IRate {
	@Override
	public double getRate() {
		return 0.5;
	}
}
